<?php

namespace TaskItPelag;

class TaskOneSolution
{
    private const YES_CHECK_RESULT = 'YES';
    private const NO_CHECK_RESULT = 'NO';

    private array $availableWords = ['output', 'out', 'puton', 'input', 'in', 'one'];

    public function solution(array $input): array
    {
        $stringCounter = $input[0];
        array_shift($input);
        $dialogueStrings = $input;

        return array_merge([$stringCounter], $this->makeSolution($stringCounter, $dialogueStrings));
    }

    private function makeSolution(int $stringCounter, array $dialogueStrings): array
    {
        $result = [];

        foreach ($dialogueStrings as $dialogueString) {
            if ($this->checkDialogue($dialogueString)) {
                $result[$dialogueString] = self::YES_CHECK_RESULT;

                continue;
            }

            $result[$dialogueString] = self::NO_CHECK_RESULT;
        }

        return $result;
    }

    private function checkDialogue(string $dialogueString): bool
    {
        $result = str_replace($this->availableWords, "", $dialogueString);

        if (strlen($result)) {
            return false;
        }

        return true;
    }

    public static function checkSolutionWithExample(): void
    {
        $input = [
            6,
            'puton',
            'inonputin',
            'oneputonininputoutoutput',
            'oneininputwooutoutput',
            'outpu',
            'utput'
        ];

        var_dump(
            (new TaskOneSolution())->solution($input)
        );
    }
}

class TaskTwoSolution
{
    public function solution(string $stringToCheck): int
    {
        $arrayOfIntegers = preg_split('/[A-Za-zА-Яа-я]/', $stringToCheck);

        return array_reduce($arrayOfIntegers, function ($sum, $arrayElem) {
            $sum += ((int)$arrayElem);

            return $sum;
        }, 0);
    }

    public static function checkSolutionWithExample(): void
    {
        var_dump(
            (new TaskTwoSolution())->solution("900коко50кокококк25коко25")
        );
    }
}

class TaskThreeSolution
{
    public function solution(int $burgersTime, int $cheeseburgersTime, int $breakTime): int
    {
        $resultForBurgers = ($breakTime / $burgersTime);
        $resultForCheeseburgers = ($breakTime / $cheeseburgersTime);

        return max($resultForBurgers, $resultForCheeseburgers);
    }

    public static function checkSolutionWithExample(): void
    {
        var_dump(
            (new TaskThreeSolution())->solution(10, 15, 500)
        );
    }
}

class SolutionChecker
{
    public static function checkAllSolutionsWithExamples(): void
    {
        TaskOneSolution::checkSolutionWithExample();
        TaskTwoSolution::checkSolutionWithExample();
        TaskThreeSolution::checkSolutionWithExample();
    }
}

SolutionChecker::checkAllSolutionsWithExamples();
