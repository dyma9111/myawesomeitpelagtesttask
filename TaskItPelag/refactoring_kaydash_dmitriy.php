<?php

namespace TaskItPelag;

use Throwable;

interface UseYearmonthFoldersSetting
{
    function getUseYearmonthFolders(): ?string;
}

interface UseWpUploads
{
    function getWpUploads(): ?string;
}

interface UseS3
{
    function getUseCopyToS3(): ?string;

    function getServeFromS3(): ?string;
}

interface UseObjectInfo
{
    const DEFAULT_OBJECT_PREFIX = 'get_default_object_prefix';

    function getObjectPrefix(): ?string;

    function getObjectVersioning(): ?string;
}

class SettingsRegistry implements UseObjectInfo, UseS3, UseWpUploads, UseYearmonthFoldersSetting
{
    function getUseYearmonthFolders(): ?string
    {
        return '2';
    }

    function getWpUploads(): ?string
    {
        return '1';
    }

    function getUseCopyToS3(): ?string
    {
        return '2';
    }

    function getServeFromS3(): ?string
    {
        return '3';
    }

    function getObjectPrefix(): ?string
    {
        return '4';
    }

    function getObjectVersioning(): ?string
    {
        return '1212';
    }
}

class SettingsRepository
{
    private const COPY_TO_S3 = 'copy-to-s3';
    private const SERVE_FROM_S3 = 'serve-from-s3';
    private const OBJECT_VERSIONING = 'object-versioning';
    private const OBJECT_PREFIX = 'object-prefix';
    private const USE_YEARMONTH_FOLDERS = 'use-yearmonth-folders';

    public function __construct(
        private readonly OptionFactory    $optionFactory,
        private readonly SettingsRegistry $settingsRegistry,
        private readonly LoggerInterface  $logger
    )
    {
    }

    public function getSetting(string $key, string $default = ''): OptionDto
    {
        if (!is_null($this->settingsRegistry->getWpUploads()) && $this->checkKeyForS3($key)) {
            return new OptionDto($key, $default);
        }

        return $this->useObjectVersioning($key, $default);
    }

    private function useObjectVersioning(string $key, string $default): OptionDto
    {
        if ($key === self::OBJECT_VERSIONING && !is_null($this->settingsRegistry->getObjectVersioning())) {
            return new OptionDto($key, $default);
        }

        if ($key === self::OBJECT_PREFIX && !is_null($this->settingsRegistry->getObjectPrefix())) {
            return new OptionDto($key, UseObjectInfo::DEFAULT_OBJECT_PREFIX);
        }

        $this->logger->logIfDebug()->logAll('log message: step 3');

        if ($key === self::USE_YEARMONTH_FOLDERS && !is_null($this->settingsRegistry->getUseYearmonthFolders())) {
            $this->optionFactory->makeOption(OptionFactory::UPLOADS_USE_YEARMONTH_FOLDERS);
        }

        return new OptionDto($key, $default);
    }

    private function checkKeyForS3(string $key): bool
    {
        return match ($key) {
            self::COPY_TO_S3, self::SERVE_FROM_S3 => true,
            default => false
        };
    }
}

class OptionDto
{
    public function __construct(
        public readonly string $key,
        public readonly string $option
    )
    {
    }

    public function __toString(): string
    {
        return sprintf("Option DTO - \$key = %s; \$option = %s", $this->key, $this->option);
    }
}

class OptionFactory
{
    public const KEY_1 = 'key1';
    public const KEY_2 = 'key2';
    public const UPLOADS_USE_YEARMONTH_FOLDERS = 'uploads_use_yearmonth_folders';
    public const DEFAULT_VALUE = 'default';

    public function __construct(
        private readonly LoggerInterface $logger
    )
    {
    }

    public function makeOption(string $key): OptionDto
    {
        $dto = match ($key) {
            self::KEY_1 => new OptionDto($key, '6'),
            self::KEY_2 => new OptionDto($key, '5'),
            self::UPLOADS_USE_YEARMONTH_FOLDERS => new OptionDto($key, '10'),
            default => new OptionDto($key, self::DEFAULT_VALUE),
        };

        $this->logger->logIfDebug()->logAll($dto);
        return $dto;
    }
}

/**
 * PROVIDERS DECLARATION
 */
interface ProviderInterface
{
    /**
     * @return void
     *
     * Register method provides ability to inform DI container about classes that must be instantiated.
     */
    public function register(): void;

    public function afterBoot(): void;
}

abstract class AbstractProvider implements ProviderInterface
{
    public function __construct(
        protected readonly DIContainer $DIContainer
    )
    {
    }
}

/**
 * Class where all existed providers should be registered.
 */
class ProvidersRegistryList
{
    /**
     * @return string[]
     * Returns list of providers for app. App will register every class from every provider, if it is possible.
     */
    public function getProviders(): array
    {
        return [
            LogProvider::class,
            AppProvider::class
        ];
    }
}

/**
 * Logger
 */
interface ObservableLoggerInterface
{
    public function log(string $message): void;
}

interface LoggerNotifier
{
    public function logAll(string $message): void;
}

interface LoggerWatcherInterface
{
    public function registerLogger(ObservableLoggerInterface $logger): void;
}

interface LoggerInterface
{
    public function logIfDebug(): LoggerNotifier;

    public function logIfNotice(): LoggerNotifier;

    public function logIfError(): LoggerNotifier;
}

interface ChangableLogLevel
{
    public function setLogLevel(LogLevel $logLevel);
}

enum LogLevel: int
{
    case DEBUG = 100;
    case NOTICE = 200;
    case ERROR = 300;
}

/**
 * Logs nothing. Returns by LoggerRepository in case if app log level priority
 * higher than asked in LoggerInterface methods.
 */
class LoggerPacifier implements LoggerNotifier
{
    /**
     * @param string $message
     * @return void
     */
    public function logAll(string $message): void
    {
    }
}

/**
 * Logs to every registered logger.
 */
class MainLoggerNotifier implements LoggerWatcherInterface, LoggerNotifier
{
    /**
     * @var ObservableLoggerInterface[]
     */
    private static array $loggers = [];

    public function logAll(string $message): void
    {
        foreach (self::$loggers as $logger) {
            $logger->log($message);
        }
    }

    public function registerLogger(ObservableLoggerInterface $logger): void
    {
        self::$loggers[] = $logger;
    }
}

class LoggerRepository implements LoggerInterface, ChangableLogLevel
{
    private readonly LogLevel $loglevel;

    private readonly LoggerNotifier $loggerPacifier;

    public function __construct(
        private readonly LoggerNotifier $loggerNotifier
    )
    {
        $this->loggerPacifier = new LoggerPacifier();
    }

    public function setLogLevel(LogLevel $logLevel)
    {
        $this->loglevel = $logLevel;
    }

    public function logIfDebug(): LoggerNotifier
    {
        if ($this->loglevel->value <= LogLevel::DEBUG) {
            return $this->loggerNotifier;
        }

        return $this->loggerPacifier;
    }

    public function logIfNotice(): LoggerNotifier
    {
        if ($this->loglevel->value <= LogLevel::NOTICE) {
            return $this->loggerNotifier;
        }

        return $this->loggerPacifier;
    }

    public function logIfError(): LoggerNotifier
    {
        if ($this->loglevel->value <= LogLevel::ERROR) {
            return $this->loggerNotifier;
        }

        return $this->loggerPacifier;
    }
}

class CustomLogger implements ObservableLoggerInterface
{
    /**
     * @var false|resource
     */
    private $file;

    public function __construct()
    {
        $this->file = fopen("applogs.txt", "a");
    }

    public function log(string $message): void
    {
        fwrite($this->file, $message);
    }

    public function __destruct()
    {
        fclose($this->file);
    }
}

/**
 * Dependency Injection
 */
class DIException extends \Exception
{
    public function __construct(
        string     $message,
        int        $code = 500,
        ?Throwable $previous = null
    )
    {
        $parentMessage = sprintf("DI exception: can not make instance of class: %s", $message);

        parent::__construct($parentMessage, $code, $previous);
    }
}

interface InterfaceBinder
{
    public function bind(string $interfaceName, string $className): void;
}

abstract class InterfaceInstantiator implements InterfaceBinder
{
    /**
     * @var array
     *
     * Our interface container
     */
    protected static array $bindedInterfaces = [];

    /**
     * @param string $interfaceName
     * @param string $className
     * @return void
     *
     * Simply binds class as interface instance
     */
    public function bind(string $interfaceName, string $className): void
    {
        self::$bindedInterfaces[$interfaceName] = $className;
    }
}

/**
 * DI container. Supports binding of interfaces and of classes by itself.
 */
class DIContainer extends InterfaceInstantiator
{
    private array $classesToInstantiate = [];
    private array $instantiatedClasses = [];

    /**
     * @param string $className
     * @param callable|null $instantiateLogic
     * @return void
     * @throws \ReflectionException
     *
     * Register class for instantiation.
     * If callable given, it must return instance of class after call.
     * DIContainer is given as first argument for callable.
     */
    public function toInstantiate(string $className, callable $instantiateLogic = null): void
    {
        if (!is_null($instantiateLogic)) {
            $instantiatedClassByUser = $instantiateLogic($this);

            $classUnderReflection = new \ReflectionClass($className);

            if ($classUnderReflection->isInterface()) {
                $classMadeAfterCallable = new \ReflectionClass($instantiatedClassByUser);

                $this->instantiatedClasses[$classMadeAfterCallable->getName()] = $instantiatedClassByUser;
            }

            $this->instantiatedClasses[$className] = $instantiatedClassByUser;
        }

        if (!in_array($className, $this->classesToInstantiate, true)) {
            $this->classesToInstantiate[] = $className;
        }
    }

    /**
     * @throws DIException
     *
     * Returns instance of class if it was registered in provider and can be instantiated
     */
    public function getClass(string $className): object
    {
        $classUnderReflection = new \ReflectionClass($className);

        if ($classUnderReflection->isInterface()) {
            if (array_key_exists($className, parent::$bindedInterfaces)) {
                $className = parent::$bindedInterfaces[$className];
            } else {
                throw new DIException(
                    sprintf("%s. Interface not binded to class.", $className)
                );
            }
        }

        if (array_key_exists($className, $this->instantiatedClasses)) {
            return $this->instantiatedClasses[$className];
        }

        $this->classesToInstantiate[] = $className;

        $this->instantiateClasses();

        if (in_array($className, $this->instantiatedClasses)) {
            return $this->instantiatedClasses[$className];
        }

        throw new DIException($className);
    }

    /**
     * Instantiate registered for instantiation classes
     */
    public function instantiateClasses(): void
    {
        foreach ($this->classesToInstantiate as $classToInstantiate) {
            $this->instantiateClass($classToInstantiate);
        }
    }

    /**
     * Instantiate registered for instantiation interfaces
     */
    public function instantiateInterfaces(): void
    {
        foreach (parent::$bindedInterfaces as $interfaceToInstantiate) {
            $this->instantiateClass($interfaceToInstantiate);
        }
    }

    /**
     * @param string $className
     * @return void
     * @throws DIException
     * @throws \ReflectionException
     *
     * Instantiation of class logic
     */
    private function instantiateClass(string $className): void
    {
        if (!array_key_exists($className, $this->instantiatedClasses)) {
            $classUnderReflection = new \ReflectionClass($className);

            if ($classUnderReflection->isInterface()) {
                $realClassName = parent::$bindedInterfaces[$className];

                if (array_key_exists($realClassName, $this->instantiatedClasses)) {
                    $realClassInstance = $this->instantiatedClasses[$realClassName];
                    $this->instantiatedClasses[$className] = $realClassInstance;

                    return;
                }

                $classUnderReflection = new \ReflectionClass($realClassName);
            }

            $classConstructor = $classUnderReflection->getConstructor();

            if (!is_null($classConstructor)) {
                $classConstructorArguments = $classConstructor->getParameters();

                $this->instantiateConstructorArguments($classUnderReflection, $classConstructorArguments);

                $argumentsForConstructor = $this->makeConstructorArgumentsArray($classConstructorArguments);

                $instantiatedClass = $classUnderReflection->newInstanceArgs($argumentsForConstructor);
            } else {
                $instantiatedClass = $classUnderReflection->newInstanceWithoutConstructor();
            }

            $this->instantiatedClasses[$classUnderReflection->getName()] = $instantiatedClass;

            array_splice(
                $this->classesToInstantiate,
                array_search($classUnderReflection->getName(), $this->classesToInstantiate),
                1
            );
        }
    }

    /**
     * Recursively instantiation of arguments.
     */
    private function instantiateConstructorArguments(
        \ReflectionClass $classUnderReflection,
        array            $classConstructorArguments
    ): void
    {
        foreach ($classConstructorArguments as $constructorArgument) {
            $constructorArgumentType = $constructorArgument->getType();

            if ($constructorArgumentType->isBuiltin()) {
                throw new DIException(
                    sprintf('%s. Scalar argument DI not implemented.', $classUnderReflection->getName())
                );
            }

            $this->instantiateClass($constructorArgumentType->getName());
        }
    }

    private function makeConstructorArgumentsArray(array $constructorArguments): array
    {
        return array_reduce(
            $constructorArguments,
            function (array $arrayWithInstantiatedArguments, \ReflectionParameter $reflectedArgument) {
                $name = $reflectedArgument->getType()->getName();

                $classUnderReflection = new \ReflectionClass($reflectedArgument->getType()->getName());

                if ($classUnderReflection->isInterface()) {
                    $name = parent::$bindedInterfaces[$classUnderReflection->getName()];
                }

                $arrayWithInstantiatedArguments[] = $this->instantiatedClasses[$name];

                return $arrayWithInstantiatedArguments;
            },
            []
        );
    }
}

/**
 * Instance of app. Simply load DI container and make it instantiate classes that registered in providers.
 * To make app register class from provider, provider should be declared in ProvidersRegistryList class.
 */
class App
{
    private DIContainer $DIContainer;
    private ProvidersRegistryList $providersRegistryList;

    public function __construct()
    {
        $this->DIContainer = new DIContainer();
        $this->providersRegistryList = new ProvidersRegistryList();
    }

    public function boot(): void
    {
        $registeredProviders = $this->providersRegistryList->getProviders();

        foreach ($registeredProviders as $provider) {
            /**
             * @var $instantiatedProvider ProviderInterface
             */
            $instantiatedProvider = new $provider($this->DIContainer);

            $instantiatedProvider->register();
        }

        $this->instantiateAll();

        foreach ($registeredProviders as $provider) {
            /**
             * @var $instantiatedProvider ProviderInterface
             */
            $instantiatedProvider = new $provider($this->DIContainer);

            $instantiatedProvider->afterBoot();
        }

        $this->instantiateAll();
    }

    public function getDIContainer(): DIContainer
    {
        return $this->DIContainer;
    }

    public function setLogLevel(LogLevel $logLevel)
    {
        /**
         * @var ChangableLogLevel $logLevelRepository
         */
        $logLevelRepository = $this->DIContainer->getClass(ChangableLogLevel::class);

        $logLevelRepository->setLogLevel($logLevel);
    }

    private function instantiateAll(): void
    {
        $this->DIContainer->instantiateClasses();
        $this->DIContainer->instantiateInterfaces();
    }
}

/**
 * PROVIDERS REALISATION
 */
class LogProvider extends AbstractProvider
{
    public function register(): void
    {
        $this->DIContainer->bind(LoggerNotifier::class, MainLoggerNotifier::class);
        $this->DIContainer->bind(LoggerWatcherInterface::class, MainLoggerNotifier::class);
        $this->DIContainer->bind(LoggerInterface::class, LoggerRepository::class);
        $this->DIContainer->bind(ChangableLogLevel::class, LoggerRepository::class);

        $this->DIContainer->toInstantiate(CustomLogger::class);
    }

    public function afterBoot(): void
    {
        /**
         * @var LoggerWatcherInterface $loggerWatcher
         */
        $loggerWatcher = $this->DIContainer->getClass(LoggerWatcherInterface::class);

        $loggerWatcher->registerLogger(
            $this->DIContainer->getClass(CustomLogger::class)
        );
    }
}

class AppProvider extends AbstractProvider
{
    public function register(): void
    {
        $this->DIContainer->bind(UseObjectInfo::class, SettingsRegistry::class);
        $this->DIContainer->bind(UseS3::class, SettingsRegistry::class);
        $this->DIContainer->bind(UseWpUploads::class, SettingsRegistry::class);
        $this->DIContainer->bind(UseYearmonthFoldersSetting::class, SettingsRegistry::class);

        $this->DIContainer->toInstantiate(SettingsRegistry::class);

        $this->DIContainer->toInstantiate(SettingsRepository::class);

        $this->DIContainer->toInstantiate(OptionFactory::class);
    }

    public function afterBoot(): void
    {
    }
}

$app = new App();
$app->boot();
$app->setLogLevel(LogLevel::NOTICE);
$dicontainer = $app->getDIContainer();
