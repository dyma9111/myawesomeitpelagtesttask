-- 1 task.
SELECT TIMESTAMPDIFF(year, MIN(st.birthday), NOW()) AS max_year
FROM Class cl
         JOIN Student_in_class sc ON cl.id = sc.class
         JOIN Student st ON sc.student = st.id
WHERE cl.name LIKE '10%';

-- 2 task.
DELETE
FROM Company
WHERE id IN
      (SELECT company
       FROM (SELECT t.company, COUNT(t.id) as counted_min_id
             FROM Trip t
             GROUP BY t.company
             HAVING COUNT(t.id) =
                    (SELECT MIN(counted_trips)
                     FROM (SELECT COUNT(tr2.id) as counted_trips
                           FROM Trip tr2
                           GROUP BY tr2.company) Tbl)) Table_with_ids_to_delete)

-- 3 task
SELECT ROUND(((SELECT COUNT(*)
               FROM (SELECT COUNT(*) as rents_or_reservations
                     FROM ((SELECT res_tbl.user_id as user_id, COUNT(user_tbl.id) as counted_user_reservations
                            FROM Reservations res_tbl
                                     JOIN Users user_tbl
                                          ON user_tbl.id = res_tbl.user_id
                            GROUP BY res_tbl.user_id
                            ORDER BY res_tbl.user_id)
                           UNION
                           (SELECT room_tbl.owner_id as user_id, COUNT(user_tbl.id) as counted_user_reservations
                            FROM Users user_tbl
                                     JOIN Rooms room_tbl ON user_tbl.id = room_tbl.owner_id
                                     JOIN Reservations res_tbl ON res_tbl.room_id = room_tbl.id
                            GROUP BY room_tbl.owner_id
                            ORDER BY room_tbl.owner_id)) Table_with_rent_and_res_count
                     GROUP BY user_id) Table_with_rents_and_reservations_by_users) / (SELECT COUNT(id)
                                                                                      FROM Users) * 100), 2) as percent